/* USER CODE BEGIN Header */
/**
 ******************************************************************************
 * File Name          : freertos.c
 * Description        : Code for freertos applications
 ******************************************************************************
 * @attention
 *
 * <h2><center>&copy; Copyright (c) 2019 STMicroelectronics.
 * All rights reserved.</center></h2>
 *
 * This software component is licensed by ST under Ultimate Liberty license
 * SLA0044, the "License"; You may not use this file except in compliance with
 * the License. You may obtain a copy of the License at:
 *                             www.st.com/SLA0044
 *
 ******************************************************************************
 */
/* USER CODE END Header */

/* Includes ------------------------------------------------------------------*/
#include "FreeRTOS.h"
#include "task.h"
#include "main.h"
#include "cmsis_os.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */     
#include "usbd_cdc_if.h"

/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
/* USER CODE BEGIN PTD */
	typedef struct
	{
	char   Value[150];
	uint8_t Source;
	} Data;
/* USER CODE END PTD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */

/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */

/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/
/* USER CODE BEGIN Variables */

/* USER CODE END Variables */
osThreadId txTaskHandle;
osMutexId spimutexHandle;
osSemaphoreId RxSemHandle;

/* Private function prototypes -----------------------------------------------*/
/* USER CODE BEGIN FunctionPrototypes */

/* USER CODE END FunctionPrototypes */

void txStart(void const * argument);

extern void MX_USB_DEVICE_Init(void);
void MX_FREERTOS_Init(void); /* (MISRA C 2004 rule 8.1) */

/**
  * @brief  FreeRTOS initialization
  * @param  None
  * @retval None
  */
void MX_FREERTOS_Init(void) {
  /* USER CODE BEGIN Init */

  /* USER CODE END Init */

  /* Create the mutex(es) */
  /* definition and creation of spimutex */
  osMutexDef(spimutex);
  spimutexHandle = osMutexCreate(osMutex(spimutex));

  /* USER CODE BEGIN RTOS_MUTEX */
	/* add mutexes, ... */
  /* USER CODE END RTOS_MUTEX */

  /* Create the semaphores(s) */
  /* definition and creation of RxSem */
  osSemaphoreDef(RxSem);
  RxSemHandle = osSemaphoreCreate(osSemaphore(RxSem), 1);

  /* USER CODE BEGIN RTOS_SEMAPHORES */
	/* add semaphores, ... */
  /* USER CODE END RTOS_SEMAPHORES */

  /* USER CODE BEGIN RTOS_TIMERS */
	/* start timers, add new ones, ... */
  /* USER CODE END RTOS_TIMERS */

  /* USER CODE BEGIN RTOS_QUEUES */
	/* add queues, ... */
  /* USER CODE END RTOS_QUEUES */

  /* Create the thread(s) */
  /* definition and creation of txTask */
  osThreadDef(txTask, txStart, osPriorityIdle, 0, 128);
  txTaskHandle = osThreadCreate(osThread(txTask), NULL);

  /* USER CODE BEGIN RTOS_THREADS */
	/* add threads, ... */
  /* USER CODE END RTOS_THREADS */

}

/* USER CODE BEGIN Header_txStart */
/**
 * @brief  Function implementing the txTask thread.
 * @param  argument: Not used
 * @retval None
 */
/* USER CODE END Header_txStart */
void txStart(void const * argument)
 {
	/* init code for USB_DEVICE */
	MX_USB_DEVICE_Init();

	/* USER CODE BEGIN txStart */
	/* Infinite loop */
	int ret;
	char buffer[64];
	int message = 0;
	int message_length;

	for (;;) {
		//osDelay(10);
		ret = SX1278_LoRaEntryRx(&SX1278, 16, 2000);
		ret = Rx_data(&SX1278, (uint8_t *) buffer);
		if (ret != 0){
		CDC_Transmit_FS((uint8_t *) buffer, sizeof(buffer));
		}
		HAL_GPIO_TogglePin(LED_GPIO_Port, LED_Pin);
		ret = 0;
	}
  /* USER CODE END txStart */
}

/* Private application code --------------------------------------------------*/
/* USER CODE BEGIN Application */

int Rx_data(SX1278_t * module, uint8_t* rxBuf) {
	int ret=0;
	vTaskSuspendAll();
	//ret = SX1278_LoRaEntryRx(&SX1278, 16, 2000);
	HAL_Delay(500);
	ret = SX1278_LoRaRxPacket(module);
	if (ret > 0) {
		SX1278_read(&SX1278, (uint8_t *) rxBuf, ret);
	}
	xTaskResumeAll();
	return ret;
}

int Tx_data(SX1278_t * module, uint8_t* txBuf, int len) {
	int ret;
	//ret = SX1278_LoRaEntryTx(&SX1278, 16, 2000);
	HAL_Delay(500);
	//CDC_Transmit_FS(len, sizeof(len));
	CDC_Transmit_FS((uint8_t *) txBuf, 8);
	ret = (SX1278_LoRaEntryTx(module, (uint8_t) len, 2000));
	//	if (ret=0)
	//		return ret;
	ret = 0;
	ret = SX1278_LoRaTxPacket(module, (uint8_t *) txBuf, len, 2000);
	return ret;
}

/* USER CODE END Application */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
