/*LORA code for Transmitting Side*/

#include <SPI.h>
#include <LoRa.h>

int counter = 0;

void setup() {
  Serial.begin(9600);
  while (!Serial);

  Serial.println("LoRa Sender");

  if (!LoRa.begin(433E6)) {
    Serial.println("Starting LoRa failed!");
    while (1);
  }

  LoRa.setTxPower(20);
  
}

void loop() {
  Serial.print("Sending packet: ");
  Serial.println(counter);

  // send packet
  //char z[] = "{\"sn":123456,"t":1538352060,"c":3149,"v":21884,"pf":98,"kw":686881,"kva":714413,"kwh":127,"e-kwh":2922}";
  LoRa.beginPacket();
  //LoRa.print("%s", z);
  LoRa.print("{\"sn\":123456,\"t\":1538352060,\"c1\":3149,\"v1\":21884,\"c2\":3149,\"v2\":21884,\"c3\":3149,\"v3\":21884,\"pf\":98,\"kw\":686881,\"kva\":714413,\"kwh\":127,\"e-kwh\":2922}");
  LoRa.print(counter);
  LoRa.endPacket();
  Serial.println("Sent packet");
  counter++;

  delay(5000);
}
